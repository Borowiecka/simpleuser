package com.user

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "users")
data class User(@PrimaryKey var id: Long,
                var name: String,
                var username: String,
                var email: String)