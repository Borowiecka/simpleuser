package com.user.userapplication.user.repository

import android.arch.lifecycle.LiveData
import android.content.Context
import android.util.Log
import com.user.User
import com.user.userapplication.user.database.UserDatabase
import com.user.userapplication.user.retrofit.Webservice
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class UserRepository(val context: Context) {

    private val webservice by lazy { Webservice.create() }
    private val db by lazy { UserDatabase.getInstance(context)!! }
    private val userDao by lazy { db.userDao()!! }
    private var disposable: Disposable? = null

    fun getUser(userId: Long): LiveData<User> {
        Log.i("user","Refresh user user id - $userId")
        refreshUser(userId)
        return userDao.load(userId)
    }

    fun updateUser(user: User) {
        Log.i("user","Update user ${user.id} ${user.username}")
        Observable.just(db)
                .subscribeOn(Schedulers.io())
                .subscribe { userDao.save(user)}
    }

    private fun refreshUser(userId: Long) {
        Observable.just(db)
                .subscribeOn(Schedulers.io())
                .subscribe {
                    if (userDao.check(userId) == 0) {
                        Log.i("user", "Downloading from internet user id - $userId")
                        downloadUser(userId)
                    }
                }
    }

    fun downloadUser(userId: Long) {
        disposable = webservice.getUser(userId.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {  updateUser(it) }

    }

    fun destroy() {
        disposable?.dispose()
    }
}

