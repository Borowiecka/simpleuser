package com.user.userapplication.user.activity

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.FragmentActivity
import com.user.userapplication.R
import com.user.userapplication.user.fragment.UserFragment

class UserActivity : FragmentActivity(){
    companion object {
        const val UID_KEY = "uid"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_activity)
        val bundle = Bundle()
        bundle.putString(UserFragment.UID_KEY, intent.getStringExtra(UID_KEY))
        val fragment = UserFragment()
        fragment.arguments = bundle
        supportFragmentManager.beginTransaction().add(R.id.user_fragment, fragment).commit()
    }
}