package com.user.userapplication.user.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.user.User

@Dao
interface UserDao {
    @Insert(onConflict = REPLACE)
    fun save(user: User)
    @Query("SELECT * FROM users WHERE id = :userId")
    fun load(userId: Long): LiveData<User>
    @Query("SELECT COUNT(id) FROM users where ID = :userId")
    fun check(userId: Long): Int
}