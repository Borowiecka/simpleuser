package com.user.userapplication.user.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.user.User
import com.user.userapplication.R
import com.user.userapplication.user.repository.UserRepository
import com.user.userapplication.user.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.user_fragment.*

class UserFragment : Fragment() {
    companion object {
        const val UID_KEY = "uid"
    }
    private val userId by lazy { arguments?.getString(UID_KEY)?.toLong() ?: 1L }
    private val viewModel by lazy { ViewModelProviders.of(this).get(UserViewModel::class.java).apply {
        init(userId, UserRepository(activity!!.applicationContext))
    }}
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val userObserver = Observer<User> { user ->
            Log.i("user","Something changed id - $userId")
            name.text = user?.name
            username.text = user?.username
            email.text = user?.email
        }
        viewModel.currentUser.observe(this, userObserver)
        button2.setOnClickListener {
            viewModel.updateUsername(usernameEdit.text.toString())
        }

        button3.setOnClickListener {
            viewModel.downloadUser(userId)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.user_fragment, container, false)

    override fun onDestroy() {
        viewModel.destroy()
        super.onDestroy()
    }
}