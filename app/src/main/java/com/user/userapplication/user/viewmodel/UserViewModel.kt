package com.user.userapplication.user.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.user.User
import com.user.userapplication.user.repository.UserRepository

class UserViewModel: ViewModel() {

    lateinit var currentUser: LiveData<User>
    private lateinit var userRepository: UserRepository

    fun init(userId: Long, userRepository: UserRepository) {
        this.userRepository = userRepository
        this.currentUser = userRepository.getUser(userId)
    }

    fun updateUsername(username: String) {
        val user = currentUser.value!!.apply {
            this.username = username
        }
        userRepository.updateUser(user)
    }

    fun downloadUser(userId: Long) {
        userRepository.downloadUser(userId)
    }

    fun destroy() {
        userRepository.destroy()
    }

}