package com.user.userapplication.user.retrofit

import com.user.User
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface Webservice {

    companion object {
        fun create(): Webservice = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .build()
                .create(Webservice::class.java)


    }
    @GET("/users")
    fun getUsers(): Observable<List<User>>

    @GET("/users/{userId}")
    fun getUser(@Path("userId") userId: String): Observable<User>

}