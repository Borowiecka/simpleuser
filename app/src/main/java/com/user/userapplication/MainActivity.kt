package com.user.userapplication

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.user.userapplication.user.activity.UserActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener {
            val intent = Intent(applicationContext, UserActivity::class.java)
            intent.putExtra(UserActivity.UID_KEY, editText.text.toString())
            startActivity(intent)
        }
    }
}
